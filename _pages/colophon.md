---
layout: page
title: Colophon
permalink: /colophon/
sponsors: true
---

### Website Copy

We stand on the shoulders of giants, both previous organisers of linux.conf.au and other events, including but not limited to:
 * [PyCon AU](https://2019.pycon-au.org/)

### Website Design

Brand for linux.conf.au by [Tania Walker](https://www.taniawalker.com)

This website is developed using free and open source software.

 * [Jekyll](https://jekyllrb.com/)
 * [Bootstrap](https://getbootstrap.com/)
 * [Project Zeppelin](https://github.com/gdg-x/zeppelin) (Jekyll configuration inspiration)
